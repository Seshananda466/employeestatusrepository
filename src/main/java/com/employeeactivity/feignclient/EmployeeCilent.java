package com.employeeactivity.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.employeeactivity.dto.EmployeeResponseDto;



@FeignClient(value = "Employee-Service", url = "http://localhost:7562/employee")
public interface EmployeeCilent {
	
	@GetMapping("/getByEmployeeDetails/{employeeCode}")
	public EmployeeResponseDto getEmployeeByCode(@RequestParam  Long employeeCode);

}
