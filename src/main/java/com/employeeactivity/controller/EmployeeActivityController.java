package com.employeeactivity.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employeeactivity.dto.EmployeeActivityDto;
import com.employeeactivity.dto.EmployeeActivityResponseDto;
import com.employeeactivity.dto.UpdateEmployeeActivityDto;
import com.employeeactivity.service.AddEmployeeActivityService;
import com.employeeactivity.service.FetchEmployeeActivityService;
import com.employeeactivity.service.RemoveEmployeeActivityService;
import com.employeeactivity.service.UpdateEmployeeActivityService;

/**
 * @author seshananda
 *
 */
@Validated
@RestController
 
public class EmployeeActivityController {
	
	@Autowired
	AddEmployeeActivityService addEmployeeActivityService;
	
	@Autowired
	FetchEmployeeActivityService fetchEmployeeActivityService;
	
	@Autowired
	RemoveEmployeeActivityService removeEmployeeActivityService;
	
	@Autowired
	UpdateEmployeeActivityService updateEmployeeActivityService;
	
	/**
	 * 
	 * @param employeeActivityDto
	 * @return
	 */
	@PostMapping("/addEmployeeActivity")
	public ResponseEntity<String> addEmployeeActivity(@Valid @RequestBody List<@Valid EmployeeActivityDto> employeeActivityDto){
		addEmployeeActivityService.addEmployeeActivity(employeeActivityDto);
		return new ResponseEntity<String>("Employee Activity Added Successfully",HttpStatus.ACCEPTED);
		
	}
	/**
	 * 
	 * @param employeeCode
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	
	@GetMapping("/getEmployeeActivities")
	public ResponseEntity<List<EmployeeActivityResponseDto>> getEmployeeActivity(@RequestParam Long employeeCode, @RequestParam int pageNumber, @RequestParam int pageSize) 
					throws Exception{
		List<EmployeeActivityResponseDto> employee=fetchEmployeeActivityService.getEmployeeActivity(employeeCode,pageNumber,pageSize);
		return new ResponseEntity<List<EmployeeActivityResponseDto>>(employee,HttpStatus.OK);
		
	}
	
	
	
	
	@GetMapping("/getactivitywithdate")
	public List<EmployeeActivityResponseDto> fetchActivityWithDate(@RequestParam  int pageNumber, @RequestParam int pageSize,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate activityDate) {
		return fetchEmployeeActivityService.fetchActivityWithDate(pageNumber, pageSize,  activityDate);
	}
	
	@GetMapping("/getallactivitywithFromandtodate")
	public List<EmployeeActivityResponseDto> fetchActivityWithFromAndToDate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate) {
		return fetchEmployeeActivityService.fetchActivityWithFromAndToDate(pageNumber, pageSize, fromDate, toDate);
	}
	
	@GetMapping("/getaactivitywithemployeecodeanddate")
	public List<EmployeeActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam Long employeeCode, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate activityDate ) {
		return fetchEmployeeActivityService.fetchActivityWithEmployeeCodeAndDate(pageNumber, pageSize, employeeCode, activityDate);
	}
	
	@GetMapping("/getactivitywithemployeecodefromandtodate")
	public List<EmployeeActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam Long employeeCode, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate) {
		return fetchEmployeeActivityService.fetchActivityWithEmployeecodeFromAndTodate(pageNumber, pageSize, employeeCode,  fromDate, toDate);
	}
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	
	@DeleteMapping("/removeEmployeeActivity")
	public ResponseEntity<String> removeEmployeeActivity(@RequestParam Long id) throws Exception{
		removeEmployeeActivityService.removeEmployeeActivity(id);
		return new ResponseEntity<String>("Employee Activity removed Successfully.",HttpStatus.OK);
		
	}
	/**
	 * 
	 * @param updateEmployeeActivityDto
	 * @return
	 * @throws Exception
	 */
	
	@PatchMapping("/updateEmployeeActivity")
	public ResponseEntity<String> updateEmployeeActivity(@RequestBody UpdateEmployeeActivityDto updateEmployeeActivityDto) throws Exception{
		updateEmployeeActivityService.updateEmployeeActivity(updateEmployeeActivityDto);
		return new ResponseEntity<String>("Employee Activity updated Successfully",HttpStatus.ACCEPTED) ;
		
	}

}
