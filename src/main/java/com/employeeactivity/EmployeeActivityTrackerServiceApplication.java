package com.employeeactivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
@EnableFeignClients
@SpringBootApplication
public class EmployeeActivityTrackerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeActivityTrackerServiceApplication.class, args);
	}

}
