package com.employeeactivity.service;

import java.time.LocalDate;
import java.util.List;

import com.employeeactivity.dto.EmployeeActivityResponseDto;
import com.employeeactivity.exceptions.EmployeeActivitiesNotFoundException;

public interface FetchEmployeeActivityService {

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @param employeeCode
	 * @return
	 * @throws EmployeeActivitiesNotFoundException 
	 * @throws Exception
	 */
	List<EmployeeActivityResponseDto> getEmployeeActivity(Long employeeCode, int pageSize, int pageNumber) throws EmployeeActivitiesNotFoundException ;

	
	List<EmployeeActivityResponseDto> fetchActivityWithDate(int pageNumber, int pageSize, LocalDate activityDate);
	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	List<EmployeeActivityResponseDto> fetchActivityWithFromAndToDate(int pageNumber, int pageSize, LocalDate fromDate,
			LocalDate toDate);
	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param employeeCode
	 * @param activityDate
	 * @return
	 */
	List<EmployeeActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate activityDate);
	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param employeeCode
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	List<EmployeeActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate fromDate, LocalDate toDate);


	
	

}
