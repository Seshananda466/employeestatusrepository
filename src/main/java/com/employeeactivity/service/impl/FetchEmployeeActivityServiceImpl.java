package com.employeeactivity.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employeeactivity.dto.EmployeeActivityResponseDto;
import com.employeeactivity.entity.EmployeeActivity;
import com.employeeactivity.exceptions.EmployeeActivitiesNotFoundException;
import com.employeeactivity.exceptions.EmployeeNotFoundException;
import com.employeeactivity.feignclient.EmployeeCilent;
import com.employeeactivity.repository.EmployeeActivityRepository;
import com.employeeactivity.service.FetchEmployeeActivityService;

@Service
public class FetchEmployeeActivityServiceImpl implements FetchEmployeeActivityService {
	private static final Logger logger = LoggerFactory.getLogger(FetchEmployeeActivityServiceImpl.class);
	@Autowired
	EmployeeActivityRepository employeeActivityRepository;

	@Autowired
	EmployeeCilent employeeClient;

	@Override
	public List<EmployeeActivityResponseDto> getEmployeeActivity(Long employeeCode, int pageNumber, int pageSize)
			throws EmployeeActivitiesNotFoundException {
		Page<EmployeeActivity> employeeActivity;
		List<EmployeeActivityResponseDto> employeeActivityResponseDtoList = new ArrayList<EmployeeActivityResponseDto>();

		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		employeeActivity = employeeActivityRepository.findByEmployeeCode(employeeCode, pageable);
		if (employeeActivity.isEmpty()) {
			logger.warn("Employee Activites not Found");
			throw new EmployeeActivitiesNotFoundException("Employee Activites not Found");
		}
		logger.info("Get the Employee Activities and its Status");
		employeeActivity.stream().forEach(employee -> {
			EmployeeActivityResponseDto employeeActivityDto = new EmployeeActivityResponseDto();
			BeanUtils.copyProperties(employee, employeeActivityDto);
			employeeActivityResponseDtoList.add(employeeActivityDto);
		});
		return employeeActivityResponseDtoList;
	}

	@Override
	public List<EmployeeActivityResponseDto> fetchActivityWithDate(int pageNumber, int pageSize,
			LocalDate activityDate) {
		// TODO Auto-generated method stub

		List<EmployeeActivityResponseDto> employeeActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		logger.info("got the db result failed to convert the date ");
		Page<EmployeeActivity> activity = employeeActivityRepository.findByactivityDate(pageable, activityDate);

		logger.info("got the db result failed to convert the date");
		if (activity.isEmpty()) {
			logger.info("list is empty");
			throw new EmployeeNotFoundException("No activities found for this date");
		}

		activity.stream().forEach(actTrack -> {
			EmployeeActivityResponseDto employeeActivityResponseDto = new EmployeeActivityResponseDto();
			BeanUtils.copyProperties(actTrack, employeeActivityResponseDto);

			employeeActivityResponseDtoList.add(employeeActivityResponseDto);

		});

		return employeeActivityResponseDtoList;

	}

	@Override
	public List<EmployeeActivityResponseDto> fetchActivityWithFromAndToDate(int pageNumber, int pageSize,
			LocalDate fromDate, LocalDate toDate) {
		// TODO Auto-generated method stub

		List<EmployeeActivityResponseDto> employeeActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<EmployeeActivity> activity = employeeActivityRepository.findByfromDateandtoDate(pageable, fromDate,
				toDate);

		logger.info("exception for employees activity");
		if (activity.isEmpty()) {
			logger.info("list is empty");
			throw new EmployeeNotFoundException("No activities found for this period");
		}

		activity.stream().forEach(actTrack -> {
			EmployeeActivityResponseDto employeeActivityResponseDto = new EmployeeActivityResponseDto();
			BeanUtils.copyProperties(actTrack, employeeActivityResponseDto);

			employeeActivityResponseDtoList.add(employeeActivityResponseDto);

		});

		return employeeActivityResponseDtoList;
	}

	@Override
	public List<EmployeeActivityResponseDto> fetchActivityWithEmployeeCodeAndDate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate activityDate) {
		// TODO Auto-generated method stub

		List<EmployeeActivityResponseDto> employeeActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);

		Page<EmployeeActivity> activity = employeeActivityRepository.findByemployeeCodeandDate(pageable, employeeCode,
				activityDate);

		logger.info("exception for employees activity");
		if (activity.isEmpty()) {
			logger.info("list is empty");
			throw new EmployeeNotFoundException("No employee found");
		}

		activity.stream().forEach(actTrack -> {
			EmployeeActivityResponseDto employeeActivityResponseDto = new EmployeeActivityResponseDto();
			BeanUtils.copyProperties(actTrack, employeeActivityResponseDto);

			employeeActivityResponseDtoList.add(employeeActivityResponseDto);

		});

		return employeeActivityResponseDtoList;
	}

	@Override
	public List<EmployeeActivityResponseDto> fetchActivityWithEmployeecodeFromAndTodate(int pageNumber, int pageSize,
			Long employeeCode, LocalDate fromDate, LocalDate toDate) {
		// TODO Auto-generated method stub

		List<EmployeeActivityResponseDto> employeeActivityResponseDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<EmployeeActivity> activity = employeeActivityRepository.findByemployeeCodewithFromAndTodate(pageable,
				employeeCode, fromDate, toDate);

		logger.info("exception for employees activity");
		if (activity.isEmpty()) {
			logger.info("list is empty");
			throw new EmployeeNotFoundException("No employee found");
		}

		activity.stream().forEach(actTrack -> {

			EmployeeActivityResponseDto employeeActivityResponseDto = new EmployeeActivityResponseDto();
			BeanUtils.copyProperties(actTrack, employeeActivityResponseDto);

			employeeActivityResponseDtoList.add(employeeActivityResponseDto);

		});

		return employeeActivityResponseDtoList;
	}

}
