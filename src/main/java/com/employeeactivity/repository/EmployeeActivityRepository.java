package com.employeeactivity.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.employeeactivity.dto.EmployeeActivityResponseDto;
import com.employeeactivity.entity.EmployeeActivity;

public interface EmployeeActivityRepository extends JpaRepository<EmployeeActivity, Long> {

	/**
	 * 
	 * @param employeeCode
	 * @param pageable
	 * @return
	 */

	Page<EmployeeActivity> findByEmployeeCode(Long employeeCode, Pageable pageable);

	/**
	 * 
	 * @param employeeCode
	 * @param activityDate
	 * @return
	 */

	List<EmployeeActivityResponseDto> findByEmployeeCodeAndActivityDate(Long employeeCode, LocalDate activityDate);

	Page<EmployeeActivity> findByactivityDate(Pageable pageable, LocalDate activityDate);
	
	//@Query(value = "select u.* From employee_activities u where u.activity_date>=:fromDate and u.activity_date<=:toDate", nativeQuery = true)
	@Query("select d from EmployeeActivity d where d.activityDate>= :fromDate and d.activityDate <= :toDate")
	Page<EmployeeActivity> findByfromDateandtoDate(Pageable pageable,@Param("fromDate") LocalDate fromDate,@Param("toDate") LocalDate toDate);

	//@Query(value = "select u.* From employee_activities u where u.employee_code>=:employeeCode and u.activity_date<=:activityDate", nativeQuery = true)
	@Query("select d from EmployeeActivity d where d.employeeCode=:employeeCode and d.activityDate =:activityDate")
	Page<EmployeeActivity> findByemployeeCodeandDate(Pageable pageable, Long employeeCode, LocalDate activityDate);

	//@Query(value = "select u.* From employee_activities u where u.employee_code>=:employeeCode and u.activity_date>=:fromDate and u.activity_date<=:toDate", nativeQuery = true)
	@Query("select d from EmployeeActivity d where d.employeeCode=:employeeCode and d.activityDate >=:fromDate and d.activityDate <= :toDate")
	Page<EmployeeActivity> findByemployeeCodewithFromAndTodate(Pageable pageable, Long employeeCode, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

}
